# Credentials for GMail

See [Google console](https://console.cloud.google.com/apis/credentials?orgonly=true&project=personal-gmail-access&supportedpurview=project),
create a credential and save to a 'credentials.json'. This is ignored by git.
