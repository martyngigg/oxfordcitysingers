#!/usr/bin/env python
# SumUp does not provide an API endpoint to view the details of an online order
# but we do receive and email confirmation of each order so we can pull this
# information from these emails. It requires acess to the GMail API to have
# been granted in the Google API console.


import argparse
import base64
from bs4 import BeautifulSoup
import pathlib
import polars as pl
import re
import sys
from xlsxwriter import Workbook

# For now we just assume the gmail module is in the parent directory
sys.path.insert(0, str(pathlib.Path(__file__).parent.parent))
from gmail import create_gmail_service
from googleapiclient.discovery import Resource as GoogleApiResource

CREDENTIALS_FILENAME = pathlib.Path(__file__).parent / "credentials.json"
SUMUP_ORDER_EMAIL = "no-reply@sumupstore.com"


class SumUpOrderConfirmationMatcher:

    def __init__(self, full_price: float, concession_price: float):
        self.ORDER_NUMBER_RE = re.compile(r"New order #(\d+)")
        self.CUSTOMER_RE = re.compile(
            r"(\d\d\d\d/\d\d/\d\d) \d\d:\d\d:\d\d\s+(?:mr|mrs|ms|dr)?(.*)$",
            re.IGNORECASE,
        )
        self.FULL_PRICE = full_price
        self.FULL_PRICE_COUNT_RE = re.compile(rf"(\d)x\s£{full_price:.2f}")
        self.CONC_PRICE = concession_price
        self.CONC_COUNT_RE = re.compile(rf"(\d)x\s£{concession_price:.2f}")


class Customer:

    @staticmethod
    def create_from_full_name(full_name: str):
        """Create a customer from a space-separated
        string. The first name is the considered to be
        first element and the remainder assigned to the last
        name.
        """
        elements = full_name.split()
        return Customer(elements[0], " ".join(elements[1:]))

    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name


class OrdersTableBuilder:
    # Maps to columns names is DataFrame & Excel
    CustomerFirstNameColumn = "First Name"
    CustomerLastNameColumn = "Last Name"
    OrderNoColumn = "Order No"
    OrderDateColumn = "Date"
    FullPriceColumn = "Full Price"
    ConcessionColumn = "Concession"
    TotalNumberColumn = "Total Number"

    def __init__(self) -> None:
        self.data = {
            self.CustomerFirstNameColumn: [],
            self.CustomerLastNameColumn: [],
            self.OrderNoColumn: [],
            self.OrderDateColumn: [],
            self.FullPriceColumn: [],
            self.ConcessionColumn: [],
        }

    def append(self, row: dict):
        for key, value in row.items():
            self.data[key].append(value)

    def to_dataframe(self) -> pl.DataFrame:
        # Add the total number of tickets per sale column
        df = pl.DataFrame(self.data)
        df = df.with_columns(
            (df[self.FullPriceColumn] + df[self.ConcessionColumn]).alias(
                self.TotalNumberColumn
            )
        )
        return df


class SummaryTable:
    TotalFullPrice = "Total Full Price"
    TotalConcession = "Total Concession"
    TotalCount = "Total Count"
    TotalSales = "Total Sales"


def parse_sumup_order_confirmation(
    order_matcher: SumUpOrderConfirmationMatcher, message, item: str
) -> dict:
    # We want the html part that is base64-encoded.
    html_part = next(
        filter(
            lambda part: part["mimeType"] == "text/html", message["payload"]["parts"]
        )
    )
    return parse_sumup_order_html(
        order_matcher,
        item,
        base64.urlsafe_b64decode(html_part["body"]["data"]).decode("UTF8"),
    )


def parse_sumup_order_html(
    order_matcher: SumUpOrderConfirmationMatcher, item: str, html: str
) -> dict:
    # The full name of the item in the shop must be somewhere or we assume it's
    # for a different item in the shop and we don't support that
    if html.find(item) < 0:
        return

    # We do our best to be as accepting of format changes as possible by just
    # searching through everything for known string sequences within a tag
    parser = BeautifulSoup(html, features="lxml")
    tags_with_strings = parser.find_all(lambda tag: tag.string is not None)

    # todo: Can this use the new case/pattern matching syntax?
    order_number, order_date, customer, full_price_count, conc_count = (
        None,
        None,
        None,
        0,
        0,
    )
    for tag in tags_with_strings:
        trimmed = str(tag.string.strip())
        if (m := order_matcher.ORDER_NUMBER_RE.match(trimmed)) is not None:
            order_number = int(m.group(1))
        elif (m := order_matcher.CUSTOMER_RE.match(trimmed)) is not None:
            # convert to ISO standard
            order_date = m.group(1).replace("/", "-")
            customer = Customer.create_from_full_name(m.group(2))
        elif (m := order_matcher.FULL_PRICE_COUNT_RE.match(trimmed)) is not None:
            full_price_count = int(m.group(1))
        elif (m := order_matcher.CONC_COUNT_RE.match(trimmed)) is not None:
            conc_count = int(m.group(1))
        else:
            continue

    return {
        OrdersTableBuilder.OrderNoColumn: order_number,
        OrdersTableBuilder.OrderDateColumn: order_date,
        OrdersTableBuilder.CustomerFirstNameColumn: customer.first_name,
        OrdersTableBuilder.CustomerLastNameColumn: customer.last_name,
        OrdersTableBuilder.FullPriceColumn: full_price_count,
        OrdersTableBuilder.ConcessionColumn: conc_count,
    }


def parse_sumup_orders(
    order_matcher: SumUpOrderConfirmationMatcher,
    messages: GoogleApiResource,
    item: str,
    after=None,
):
    query_args = [f"from:{SUMUP_ORDER_EMAIL}"]
    if after is not None:
        query_args.append(f"after:{after}")
    query = messages.list(userId="me", includeSpamTrash=False, q=" ".join(query_args))
    print(f"Querying email with args {query_args}")
    results = query.execute()
    if "messages" not in results:
        return

    builder = OrdersTableBuilder()
    for msg in results["messages"]:
        if order := parse_sumup_order_confirmation(
            order_matcher,
            messages.get(userId="me", id=msg["id"], format="full").execute(),
            item,
        ):
            builder.append(order)

    return builder.to_dataframe()


def summarise(
    order_matcher: SumUpOrderConfirmationMatcher, orders: pl.DataFrame
) -> pl.DataFrame:
    """Create a table summarise order information"""
    total_full_price, total_concession = (
        orders[OrdersTableBuilder.FullPriceColumn].sum(),
        orders[OrdersTableBuilder.ConcessionColumn].sum(),
    )
    total_count = total_full_price + total_concession
    total_sales = total_full_price * order_matcher.FULL_PRICE
    if order_matcher.CONC_PRICE is not None:
        total_sales += total_concession * order_matcher.CONC_PRICE

    return pl.DataFrame(
        {
            SummaryTable.TotalFullPrice: total_full_price,
            SummaryTable.TotalConcession: total_concession,
            SummaryTable.TotalCount: total_count,
            SummaryTable.TotalSales: total_sales,
        }
    )


def group_by_date(orders: pl.DataFrame) -> pl.DataFrame:
    """Create a table with the total number of tickets by date"""

    return (
        orders.select(
            pl.col(OrdersTableBuilder.OrderDateColumn).str.to_date(),
            (
                pl.col(OrdersTableBuilder.FullPriceColumn)
                + pl.col(OrdersTableBuilder.ConcessionColumn)
            ).alias(SummaryTable.TotalCount),
        )
        .group_by(pl.col(OrdersTableBuilder.OrderDateColumn))
        .sum()
    )


def save_barchart(df: pl.DataFrame, filepath: str):
    df.plot.bar().encode(
        x=OrdersTableBuilder.OrderDateColumn, y=SummaryTable.TotalCount
    ).save(filepath)


def sort_orders(orders: pl.DataFrame) -> pl.DataFrame:
    return orders.sort(OrdersTableBuilder.CustomerLastNameColumn)


def write(filepath: str, orders: pl.DataFrame, orders_summary: pl.DataFrame):
    with Workbook(filepath) as wb:
        orders.write_excel(
            wb,
            worksheet="Online Orders",
            autofit=True,
            column_formats={OrdersTableBuilder.OrderDateColumn: "dd/mm/yyyy"},
            table_style="Table Style Medium 2",
        )
        orders_summary.write_excel(
            wb,
            worksheet="Summary",
            autofit=True,
            column_formats={SummaryTable.TotalSales: "£0.00"},
        )


def parse_main_args():
    parser = argparse.ArgumentParser(
        description="Parse SumUp concert order messages in Gmail."
    )
    parser.add_argument("item", help="The full name of the item in the SumUp shop.")
    parser.add_argument(
        "full_price", default=0.0, type=float, help="The price of a full-price ticket"
    )
    parser.add_argument(
        "concession_price",
        default=0.0,
        type=float,
        nargs="?",
        help="The price of concession ticket if applicable.",
    )

    parser.add_argument(
        "--after",
        help="If supplied, restrict messages to after this date. Format=YY/MM/DD",
    )
    parser.add_argument(
        "--output",
        dest="output",
        help="If supplied write a CSV file to the given location. Any existing files are overwritten.",
    )
    parser.add_argument(
        "--plot",
        dest="plot",
        help="If supplied write a png containing a bar chart of sales by date.",
    )
    return parser.parse_args()


def main():
    args = parse_main_args()
    gmail = create_gmail_service(CREDENTIALS_FILENAME)

    order_matcher = SumUpOrderConfirmationMatcher(
        args.full_price, args.concession_price
    )
    orders = parse_sumup_orders(
        order_matcher, gmail.users().messages(), item=args.item, after=args.after
    )
    if orders.is_empty():
        print(f"No orders found for '{args.item}' from '{SUMUP_ORDER_EMAIL}'.")
        return

    orders_grouped = group_by_date(orders)
    print(orders_grouped)
    if args.plot is not None:
        save_barchart(orders_grouped, filepath=args.plot)
        print(f"Sales by date chart saved to '{args.plot}'")
    orders_summary = summarise(order_matcher, orders)
    print(orders_summary)

    if args.output is not None:
        # Writing to an existing sheet seems to cause some sort of merge issue.
        # Ensure a fresh sheet each time.
        filename = pathlib.Path(args.output)
        if filename.exists():
            filename.unlink()
        orders = sort_orders(orders)
        write(filename, orders, orders_summary)
        print(f"Orders and summary spreadsheet saved to '{args.output}'")


main()
