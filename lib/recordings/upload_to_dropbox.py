"""Perform actions on the Singer's Dropbox account"""
from __future__ import (absolute_import, print_function)

import os
import pprint

import click
import dropbox
import dropbox.sharing


@click.command()
@click.argument('token')
@click.argument('srcdir')
@click.argument('destdir')
def main(token, srcdir, destdir):
    dbx = dropbox.Dropbox(token)

    print("Uploading files in {}".format(srcdir))
    for item in os.listdir(srcdir):
        print("uploading {}".format(item))
        dest = os.path.join(destdir, item)
        with open(os.path.join(srcdir, item), 'rb') as f:
            dbx.files_upload(f.read(), dest, mute=True)


if __name__ == "__main__":
    main()
