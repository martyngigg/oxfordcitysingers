#! /usr/bin/env python
"""Check mail account for new songs and download them.
"""
# system
import argparse
import base64
import os
import json
import re
import sys

# 3rd party
from googleapiclient.discovery import build
from googleapiclient import errors
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials

# constants
TOKEN_FILENAME = 'token.json'
SCOPES = ['https://www.googleapis.com/auth/gmail.readonly']
CREDENTIALS_FILENAME = 'credentials.json'


def create_gmail_service():
    """Create a GMail service object using the given credentials
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists(TOKEN_FILENAME):
        creds = Credentials.from_authorized_user_file(TOKEN_FILENAME, SCOPES)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                CREDENTIALS_FILENAME, SCOPES)
            creds = flow.run_local_server(port=55802)
        # Save the credentials for the next run
        with open(TOKEN_FILENAME, 'w') as token:
            token.write(creds.to_json())

    return build('gmail', 'v1', credentials=creds)


def get_attachments(service, user_id, msg_id, store_dir, filter='.*'):
    """Get and store attachment from Message with given id.

    Args:
      service: Authorized Gmail API service instance.
      user_id: User's email address. The special value "me"
      can be used to indicate the authenticated user.
      msg_id: ID of Message containing attachment.
      store_dir: The directory used to store attachments.
      filter: An optional regex to restrict the files downloaded
    """
    if not os.path.exists(store_dir):
        os.makedirs(store_dir)
    filename_filter_re = re.compile(filter)
    try:
        message = service.users().messages().get(userId=user_id,
                                                 id=msg_id).execute()

        for part in message['payload']['parts']:
            filename = part['filename']
            if filename:
                if filename_filter_re.match(filename):
                    print('  saving attachment "{}" to "{}"'.format(
                        filename, store_dir))
                else:
                    print('  ignoring "{}" that does not match filter'.format(
                        filename))
                    continue
                if 'data' in part['body']:
                    file_data = part['body']['data']
                else:
                    att_id = part['body']['attachmentId']
                    att = service.users().messages().attachments().get(
                        userId=user_id, messageId=msg_id, id=att_id).execute()
                    file_data = att['data']
                file_data = base64.urlsafe_b64decode(file_data.encode('UTF-8'))
                path = ''.join([store_dir, filename])
                f = open(path, 'wb')
                f.write(file_data)
                f.close()

    except errors.HttpError as error:
        print('An error occurred: %s' % error)


def parse_main_args(args):
    parser = argparse.ArgumentParser(
        description='Download attachments from gmail')
    parser.add_argument('sender')
    parser.add_argument(
        '--after',
        dest='after',
        help='If supplied restrict messages to after this date. Format=YY/MM/DD'
    )
    parser.add_argument(
        '--filter',
        dest='filter',
        default='.*',
        help='An optional regex used to restrict the attachments downloaded')
    parser.add_argument('--output-dir',
                        dest='output_dir',
                        default='./',
                        help='Directory used to store the attachments')
    return parser.parse_args(args=args)


def main(argv=sys.argv):
    args = parse_main_args(argv[1:])

    print('Authenticating with Gmail')
    service = create_gmail_service()
    query_args = []
    if args.sender:
        query_args.append('from:' + args.sender)
    if args.after:
        query_args.append('after:' + args.after)

    query = service.users().messages().list(userId='me',
                                            includeSpamTrash=False,
                                            q=' '.join(query_args))
    print('Querying email for messages')
    results = query.execute()
    if 'messages' in results:
        print('Found {} messages matching query'.format(
            len(results['messages'])))
        for msg_info in results['messages']:
            get_attachments(service,
                            user_id='me',
                            msg_id=msg_info['id'],
                            store_dir=args.output_dir,
                            filter=args.filter)
    else:
        print('No messages found that match the given query')


if __name__ == '__main__':
    main()
