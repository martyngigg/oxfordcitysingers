"""Helper functions to authenicate with Google Mail. It expects the
credentials.json file to be next to this module.
"""
# system
import argparse
import base64
import os
import pathlib
import json
import re
import sys

# 3rd party
from googleapiclient.discovery import build
from googleapiclient import errors
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials

# constants
TOKEN_FILENAME = 'token.json'
SCOPES = ['https://www.googleapis.com/auth/gmail.readonly']


def create_gmail_service(credentials_filename):
    """Create a GMail service object using the given credentials
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists(TOKEN_FILENAME):
        creds = Credentials.from_authorized_user_file(TOKEN_FILENAME, SCOPES)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                credentials_filename, SCOPES)
            creds = flow.run_local_server(port=55802)
        # Save the credentials for the next run
        with open(TOKEN_FILENAME, 'w') as token:
            token.write(creds.to_json())

    return build('gmail', 'v1', credentials=creds)
