
#! /bin/bash
# Starts the stack locally with all of the defaults for passwords and
# a dump of the database that has localhost as the hostname
#
# DO NOT USE FOR PRODUCTION
SCRIPTPATH=$(cd "$(dirname "$0")"; pwd -P)
SOURCE_DIR=$(cd "$SCRIPTPATH" && cd .. && pwd -P)

COMPOSE_FILE_BASE=${SOURCE_DIR}/stack.yml
COMPOSE_FILE_LH=${SOURCE_DIR}/stack-localhost.yml
PROJECT_NAME=ocswebsite-dev

# We want development mode
export APPLICATION_ENV=development
export HOST_PORT=8000
export SITE_URL=http://localhost:${HOST_PORT}
export DB_DUMP=./docker/wordpress/initdb/20200905T112659-scw-f9309c.sql

# Build services
docker-compose --file ${COMPOSE_FILE_BASE} --file ${COMPOSE_FILE_LH} --project-name ${PROJECT_NAME} build
# Bring up the stack
docker-compose --file ${COMPOSE_FILE_BASE} --file ${COMPOSE_FILE_LH} --project-name ${PROJECT_NAME} up -d
