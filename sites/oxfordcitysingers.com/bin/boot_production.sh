
#! /bin/bash
# Starts the stack assuming it is the production server.
#
SCRIPTPATH=$(cd "$(dirname "$0")"; pwd -P)
SOURCE_DIR=$(cd "$SCRIPTPATH" && cd .. && pwd -P)

COMPOSE_FILE=${SOURCE_DIR}/stack.yml
PROJECT_NAME=ocswebsiteletsencrypt

# Run on port 80
export HOST_PORT=80
export SITE_URL=https://www.oxfordcitysingers.com

# Use production database on first initialization. Path should be relative
# to SOURCE_DIR
export DB_DUMP=./docker/wordpress/initdb/20200905T112659-scw-f9309c.sql

export MYSQL_ROOT_PASSWORD \
  WORDPRESS_DB_NAME \
  WORDPRESS_DB_USER \
  WORDPRESS_DB_PASSWORD \
  WORDPRESS_AUTH_KEY \
  WORDPRESS_SECURE_AUTH_KEY \
  WORDPRESS_LOGGED_IN_KEY \
  WORDPRESS_NONCE_KEY \
  WORDPRESS_AUTH_SALT \
  WORDPRESS_SECURE_AUTH_SALT \
  WORDPRESS_LOGGED_IN_SALT \
  WORDPRESS_NONCE_SALT \
  WORDPRESS_DEBUG

# Build services
docker-compose --file ${COMPOSE_FILE} --project-name ${PROJECT_NAME} build
# Bring up the stack
docker-compose --file ${COMPOSE_FILE} --project-name ${PROJECT_NAME} up -d
