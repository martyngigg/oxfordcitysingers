#! /bin/bash
# Stops the development stack.
#
# DO NOT USE FOR PRODUCTION
SCRIPTPATH=$(cd "$(dirname "$0")"; pwd -P)
SOURCE_DIR=$(cd "$SCRIPTPATH" && cd .. && pwd -P)

COMPOSE_FILE=${SOURCE_DIR}/stack.yml
PROJECT_NAME=ocswebsite-dev

# We want development mode
export APPLICATION_ENV=development
docker-compose --file ${COMPOSE_FILE} --project-name ${PROJECT_NAME} down
docker volume rm ${PROJECT_NAME}_wp_data
docker volume rm ${PROJECT_NAME}_swag_data
docker volume rm ${PROJECT_NAME}_db_data
