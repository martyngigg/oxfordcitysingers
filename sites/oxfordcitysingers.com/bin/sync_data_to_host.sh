#! /bin/bash
# Updating wordpress, core + plugins, is most easily done via the web interface.
# This script copies updates to the wordpress content back to this repository.
THISDIR=$(cd "$(dirname "$0")"; pwd -P)
SITEROOTDIR=$(dirname $THISDIR)
PROJECT_NAME=ocswebsiteletsencrypt
CONTAINER_NAME=${PROJECT_NAME}_wordpress_1

# Content to copy
WP_CONTENTDIR_CONT=/var/www/html/wp-content
WP_CONTENTDIR_HOST=$SITEROOTDIR

# Make sure current git repo is up to date
git pull --rebase

# Copy
docker cp ${CONTAINER_NAME}:${WP_CONTENTDIR_CONT} ${SITEROOTDIR}

# Add to scm
git add ${SITEROOTDIR}
git commit -m"Update wp-content from container"
