#! /bin/bash
# Stops the production stack. Use --clean-wp to prune wordpress volume
#
SCRIPTPATH=$(cd "$(dirname "$0")"; pwd -P)
SOURCE_DIR=$(cd "$SCRIPTPATH" && cd .. && pwd -P)

COMPOSE_FILE=${SOURCE_DIR}/stack.yml
PROJECT_NAME=ocswebsiteletsencrypt

export HOST_PORT=80
export SITE_URL=https://www.oxfordcitysingers.com

export MYSQL_ROOT_PASSWORD WORDPRESS_DB_NAME WORDPRESS_DB_USER WORDPRESS_DB_PASSWORD
export WORDPRESS_AUTH_KEY \
  WORDPRESS_SECURE_AUTH_KEY \
  WORDPRESS_LOGGED_IN_KEY \
  WORDPRESS_NONCE_KEY \
  WORDPRESS_AUTH_SALT \
  WORDPRESS_SECURE_AUTH_SALT \
  WORDPRESS_LOGGED_IN_SALT \
  WORDPRESS_NONCE_SALT

# Stop the stack
docker-compose --file ${COMPOSE_FILE} --project-name ${PROJECT_NAME} down

# Clean any requested volumes
if [ "$1" == "--clean-wp" ]; then
  # the suffix here needs to match the named volume in the compose file
  docker volume rm ${PROJECT_NAME}_wp_data
fi

if [ "$1" == "--clean-db" ]; then
  # the suffix here needs to match the named volume in the compose file
  docker volume rm ${PROJECT_NAME}_db_data
fi

if [ "$1" == "--clean-swag" ]; then
  # the suffix here needs to match the named volume in the compose file
  docker volume rm ${PROJECT_NAME}_swag_data
fi
