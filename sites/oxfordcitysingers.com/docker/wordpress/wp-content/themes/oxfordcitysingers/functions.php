<?php

// Set main url
update_option( 'siteurl', getenv( 'SITE_URL' ) );
update_option( 'home', getenv( 'SITE_URL' ) );


function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

function oxfordcitysingers_remove_admin_bar() {
  if (!current_user_can('administrator') && !is_admin()) {
    show_admin_bar(false);
  }
}
add_action( 'after_setup_theme', 'oxfordcitysingers_remove_admin_bar' );

function oxfordcitysingers_add_login_logout_link($items, $args) {
  if( is_user_logged_in() ) {
    $membersitem = '<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children"><a href="'. get_permalink(146)  .'">Members Area</a>';
    $membersitem .= '<ul class="sub-menu">';
    $membersitem .= '<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="'. wp_logout_url($_SERVER['REQUEST_URI']) .'">Log Out</a></li></ul></li>';
  } else {
    $membersitem = '<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="'. wp_login_url(get_permalink(146)) .'">Log In</a>';
  }
  $items .= $membersitem;
  return $items;
}
add_filter('wp_nav_menu_items', 'oxfordcitysingers_add_login_logout_link', 10, 2);

function get_template_name () {
  foreach ( debug_backtrace() as $called_file ) {
    foreach ( $called_file as $index ) {
      if ( !is_array($index[0]) AND strstr($index[0],'/themes/') AND !strstr($index[0],'footer.php') ) {
        $template_file = $index[0] ;
      }
    }
  }
  $template_contents = file_get_contents($template_file) ;
  preg_match_all("(Template Name:(.*)\n)siU",$template_contents,$template_name);
  $template_name = trim($template_name[1][0]);
  if ( !$template_name ) { $template_name = '(default)' ; }
  $template_file = array_pop(explode('/themes/', basename($template_file)));
  return $template_file . ' > '. $template_name ;
}

?>
