<div class="clear"></div>
<div class="footer">
    <?php
    /* A sidebar in the footer? Yep. You can can customize
     * your footer with four columns of widgets.
     */
    get_sidebar('footer');
    ?>
</div>
<div class="clear"></div>
<div class="bottom_footer_content">
    <div class="grid_12 alpha">         
        <div class="copyrightinfo">
            <p class="copyright">
	    &copy; 2015 Oxford City Singers
            <span class="sep">&nbsp;&#8226;&nbsp;</span>
	    Charity registered in England (1161103)
        </div>
    </div>
    <div class="grid_12 omega">
        <div class="copyrightinfo themeinfo">
            <p class="copyright themelinks">
	    <a href="http://wordpress.org/" rel="generator"><?php _e('Powered by WordPress', 'compass');?></a>
            <span class="sep">&nbsp;&#8226;&nbsp;</span>
            <?php
              printf(__('Theme based on %1$s by %2$s', 'compass'), 'Compass', '<a href="http://inkthemes.com/" rel="designer">InkThemes</a>');
            ?>
            </p>
       </div>
    </div>
</div>
</div>
</div>
<div class="clear"></div>
</div>
<?php wp_footer(); ?>
</body>
</html>
