#!/bin/sh
# On first run generate certificate chain for the domains
# specififed by the MINICA_DOMAINS environment variable

set -e

if [ -z "$MINICA_DOMAINS" ]; then
  echo "Domains not specified. Set MINICA_DOMAINS environment variable to comma-separated list of domains."
  exit 1
fi

# Generate root and domain certificates
minica --domains "$MINICA_DOMAINS"
