<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 */
?>
<?php get_header(); ?>
<div id="custom-header">
  <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/header.jpg" />
</div>

<div class="home_wrapper">
    <div class="feature-content">
        <div class="grid_8 f_feature alpha">
            <div class="feature-content-inner first">
                <!-- *** Three column Box 1 *** -->
                <?php if (compass_get_option('compass_font_icon1') != '') { ?>
                    <p class="font_icon"><i class="<?php
                        echo stripslashes(compass_get_option('compass_font_icon1'));
                        ?> fa"></i></p>

                <?php } elseif (compass_get_option('compass_fimg1') != '') { ?>
                    <div class="feature-image first">
                        <a href="<?php echo compass_get_option('compass_feature_link1'); ?>">
                            <img src="<?php echo compass_get_option('compass_fimg1'); ?>" alt="Feature image" /></a>
                    </div>
                <?php } else { ?>

                    <p class="font_icon"><i class="fa-cloud fa"> <?php } ?> </i></p>

                <?php if (compass_get_option('compass_feature_head1') != '') { ?>


                    <a href="<?php
                    if (compass_get_option('compass_feature_link1') != '') {
                        echo stripslashes(compass_get_option('compass_feature_link1'));
                    } else {
                        echo "#";
                    }
                    ?>"><h2><?php echo stripslashes(compass_get_option('compass_feature_head1')); ?></h2></a>
                   <?php } else { ?>
                    <a href="#"><h2><?php _e('Use Font Awesome Icons', 'compass'); ?></h2></a>
                <?php } if (compass_get_option('compass_feature_desc1') != '') { ?>
                    <p><?php echo stripslashes(compass_get_option('compass_feature_desc1')); ?></p>
                <?php } else { ?>
                    <p><?php _e('Go to Font Awesome and pick the icon of your choice. Copy the class of icon and past it in Theme Option Panel.', 'compass'); ?></p>
                <?php } ?>
            </div>
        </div>
        <div class="grid_8 f_feature alpha">
            <div class="feature-content-inner second">
                <!-- *** Three column Box 2 *** -->
                <?php if (compass_get_option('compass_font_icon2') != '') { ?>
                    <p class="font_icon"><i class="<?php
                    echo stripslashes(compass_get_option('compass_font_icon2'));
                    ?> fa"></i></p>

                                        <?php } elseif (compass_get_option('compass_fimg2') != '') { ?>
                    <div class="feature-image second">
                        <a href="<?php echo compass_get_option('compass_feature_link2'); ?>">
                            <img src="<?php echo compass_get_option('compass_fimg2'); ?>" alt="Feature image" /></a>
                    </div>
<?php } else { ?>

                    <p class="font_icon"><i class="fa-rocket fa"> <?php } ?> </i></p>


<?php if (compass_get_option('compass_feature_head2') != '') { ?>
                    <a href="<?php
                    if (compass_get_option('compass_feature_link2') != '') {
                        echo stripslashes(compass_get_option('compass_feature_link2'));
                    } else {
                        echo "#";
                    }
                    ?>"><h2><?php echo stripslashes(compass_get_option('compass_feature_head2')); ?></h2></a>
                <?php } else { ?>
                    <a href="#"><h2><?php _e('Amazing Animation', 'compass'); ?></h2></a>
                   <?php } if (compass_get_option('compass_feature_desc2') != '') { ?>
                    <p><?php echo stripslashes(compass_get_option('compass_feature_desc2')); ?></h2>
                <?php } else { ?>
                    <p><?php _e('Animate is a simple CSS library that saves you from writing a few more lines of CSS to animate elements on your website.', 'compass'); ?></p>
                    <?php } ?>
            </div>
        </div>
        <div class="grid_8 f_feature alpha">
            <div class="feature-content-inner third">
                <!-- *** Three column Box 3 *** -->
<?php if (compass_get_option('compass_font_icon3') != '') { ?>
                    <p class="font_icon"><i class="<?php
                    echo stripslashes(compass_get_option('compass_font_icon3'));
                    ?> fa"></i></p>

                    <?php } elseif (compass_get_option('compass_fimg3') != '') { ?>
                    <div class="feature-image third">
                        <a href="<?php echo compass_get_option('compass_feature_link3'); ?>">
                            <img src="<?php echo compass_get_option('compass_fimg3'); ?>" alt="Feature image" /></a>
                    </div>
<?php } else { ?>

                    <p class="font_icon"><i class="fa-wordpress fa"> <?php } ?> </i></p>



<?php if (compass_get_option('compass_feature_head3') != '') { ?>
                    <a href="<?php
    if (compass_get_option('compass_feature_link3') != '') {
        echo stripslashes(compass_get_option('compass_feature_link3'));
    } else {
        echo "#";
    }
    ?>"><h2><?php echo stripslashes(compass_get_option('compass_feature_head3')); ?></h2></a>
                <?php } else { ?>
                    <a href="#"><h2><?php _e('Completely responsive', 'compass'); ?></h2></a>
                   <?php } if (compass_get_option('compass_feature_desc3') != '') { ?>
                    <p><?php echo stripslashes(compass_get_option('compass_feature_desc3')); ?></h2>
                <?php } else { ?>
                    <p><?php _e('Theme is completely responsive in nature, Mobile Friendly and compatible with all the latest versions of WordPress.', 'compass'); ?></p>
                    <?php } ?>
            </div>
        </div>
    </div>
    <div class="clear"></div>
    <div class="page_content home">
        <div class="grid_17 alpha">
            <div class="content_bar gallery">
	    <?php
             $page = get_page_by_title( 'Home' );
	     echo apply_filters('the_content', $page->post_content);
	    ?>
            </div>
        </div>
        <div class="grid_7 sidebar_grid omega">
            <!--Start Sidebar-->
			<?php get_sidebar(); ?>
            <!--End Sidebar-->
        </div>
    </div>
    <div class="clear"></div>
</div>
<?php get_footer(); ?>
